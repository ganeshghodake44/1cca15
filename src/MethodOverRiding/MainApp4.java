package MethodOverRiding;

import java.util.Scanner;

public class MainApp4 {
    public static void main (String []args){
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER NO OF TICKETS");
        int qty=sc1.nextInt();
        System.out.println("ENTER TICKET PRICE");
        double price=sc1.nextDouble();
        System.out.println("SELECT THE MULTIPLEX TYPE");
        System.out.println("1:pvr\n2:_2D");
        int choice=sc1.nextInt();
        if(choice==1){
            Pvr p1=new Pvr();
            p1.bookTickets(qty,price);
        }
        else if(choice==2){
            _2D d1=new _2D();
            d1.bookTickets(qty,price);
        }
        else{
            System.out.println("INVALID CHOICE");
        }
    }
}
