package MethodOverRiding;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER PRICE");
        double price=sc1.nextDouble();
        System.out.println("SELECT MOBILE RECHARGE APPLICATION");
        System.out.println("1:PhonePay\n2:GooglePay");
        int choice=sc1.nextInt();
        if(choice==1){
            PhonePay p1=new PhonePay();
            p1.recharge(price);
        }else if(choice==2){
            GooglePay g1=new GooglePay();
            g1.recharge(price);
        }else {
            System.out.println("INVALID CHOICE");
        }
    }
}
