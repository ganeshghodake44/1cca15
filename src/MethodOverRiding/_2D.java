package MethodOverRiding;
// subclass
public class _2D extends Theaters{
    void bookTickets(int qty,double price){
        //discount on tickets is 10%
        double total=qty*price;
        double finalAmt=total-total*0.1;
        System.out.println("TOTAL AMOUNT:"+finalAmt);
    }
}
