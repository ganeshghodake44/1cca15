package MethodOverRiding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER QTY");
        int qty=sc1.nextInt();
        System.out.println("ENTER PRICE");
        double price=sc1.nextDouble();
        System.out.println("select ECOMMERCE PLATFORM");
        System.out.println("1:Flipkart\n2:Amazon");
        int choice=sc1.nextInt();
        if(choice==1){
            Flipkart f1=new Flipkart();
            f1.sellProduct(qty,price);
        }else if(choice==2){
            Amazon a1=new Amazon();
            a1.sellProduct(qty,price);
        }
        else {
            System.out.println("INVALID CHOICE");
        }
    }
}
