package MethodOverRiding;
//subclass
public class Amazon extends Ecommerce{
    void sellProduct(int qty,double price){
        //5 %discount
        double total=qty*price;
        double finalAmt=total-total*0.05;
        System.out.println("FINAL AMOUNT:"+finalAmt);
    }
}
