package MethodOverRiding;
//subclass
public class Flipkart extends Ecommerce{
    void sellProduct(int qty,double price){
        //10% discount
        double total=qty*price;
        double finalAmt=total-total*0.1;
        System.out.println("FINAL AMOUNT:"+finalAmt);
    }
}
