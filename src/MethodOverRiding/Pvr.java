package MethodOverRiding;
//subclass
public class Pvr extends Theaters{
    void bookTickets(int qty ,double price){
        //total  discount on ticket is 5%
        double total=qty*price;
        double finalAmt=total-total*0.05;
        System.out.println("TOTAL AMOUNT:"+finalAmt);
    }
}
