package Array;

import java.util.Scanner;

public class ArrayDemo14 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total numbers of floor");
        int floors= sc1.nextInt();
        System.out.println("Enter total number of flats on each floors");
        int flats=sc1.nextInt();
        int [][]data=new int[floors][flats];
        System.out.println("ENTER"+"\t"+(floors*flats)+"\t"+"flats NOS");
        //ACCEPT FLATS NOS
        for(int a=0;a<floors;a++){
            for(int b=0;b<flats;b++){
                data[a][b]=sc1.nextInt();
            }
        }
        System.out.println("====================================================");
        for(int a=0;a<floors;a++){
            System.out.println("floors no:"+(a+1));
            System.out.println("-------------------------------------------------");
            for(int b=0;b<flats;b++){
                System.out.println("flats no:"+data[a][b]+"\t");
            }
            System.out.println();
            System.out.println("===================================================");
        }
    }
}
