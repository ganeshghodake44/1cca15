package Array;

public class BillCalculator {
        void calculateBill(double[]amounts) {
            //store gst values
            double[] gstValues = gstCalculation(amounts);
            //create new array to calculate total amounts
            double[] totalAmounts = new double[amounts.length];
            //perform sum of amounts and gst values
            for(int a=0; a<amounts.length;a++) {
                totalAmounts[a] = amounts[a] + gstValues[a];
            }
            double totalBillAmount = 0.0;
            double totalgstBill = 0.0;
            double totalfinalAmount = 0.0;

            //sum of Array element
            for(int a=0;a<amounts.length;a++){
                totalBillAmount+=amounts[a];
                totalgstBill+=gstValues[a];
                totalfinalAmount+=totalAmounts[a];
            }
            // present final output to users
            System.out.println("BIL.AMT\tGST.AMT\tTOTAL");
            System.out.println("============================================");

         for(int a=0;a<amounts.length;a++){
                System.out.println(amounts[a]+"\t"+gstValues[a]+"\t"+totalAmounts[a]);
        }
        System.out.println("=============================================");
        System.out.println(totalBillAmount+"\t"+totalgstBill+"\t"+totalfinalAmount);
}
        double[]gstCalculation(double[]amounts){
        //create a new array to store gst amount
        double []gstAmounts=new double[amounts.length];
        for(int a=0;a<amounts.length;a++){
            if(amounts[a]<500){
                gstAmounts[a]=amounts[a]*0.05;  //5%gst
            }else{
                gstAmounts[a]=amounts[a]*0.1;   //10%gst
            }
        }
        return gstAmounts;
            }
    }
