package Modifiers1;

public class MainApp {
    public static void main(String[] args) {
        Master m1=new Master();
       // System.out.println("A:"+Master.a); // private so cant access

        System.out.println("B:"+m1.b);       //default(package level)

        System.out.println("C:"+m1.c);       //Protected

        System.out.println("D:"+m1.d);       //public
    }
}
