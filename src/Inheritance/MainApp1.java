package Inheritance;

public class MainApp1 {
    public static void main(String[] args) {
        PermanentEmployee p=new PermanentEmployee();

        p.getInfo(101,30000);
        p.getDesignation("ANALYST");

        System.out.println("===================================");

        ContractEmployee c=new ContractEmployee();
        c.getInfo(102,40000);
        c.getContractDetails(24);
    }
}
