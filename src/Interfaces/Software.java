package Interfaces;

public class Software extends FrontEnd implements BackEnd,Database{

    @Override
    public void developServerProgram(String language) {
        System.out.println("Developing server using:"+language);
    }

    @Override
    public void designDatabase(String dbVender) {
        System.out.println("Designing Database using:"+dbVender);
    }
}
