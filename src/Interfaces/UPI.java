package Interfaces;
//super class
@FunctionalInterface
public interface UPI {
    void transferAmount(double amt);
}
