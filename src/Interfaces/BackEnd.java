package Interfaces;
//super interface
@FunctionalInterface
public interface BackEnd {
    void developServerProgram(String language);
}
