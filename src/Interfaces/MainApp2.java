package Interfaces;

public class MainApp2 {
    public static void main(String[] args) {
        CreditCard c;
        c=new Visa();              //upcasting
        c.getType();
        c.withdraw(2500);
        System.out.println("============================");
        c=new MasterCard();       //upcasting
        c.getType();
        c.withdraw(2000);
    }
}
