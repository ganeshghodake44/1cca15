package Interfaces;
//subclass

//*******MULTIPLE INHERITANCE
//IMPLEMENTATION CLASS
public class PhonePe implements UPI,Wallet {

    @Override
    public void transferAmount(double amt) {
        System.out.println("TRANSFER AMOUNT:"+amt);
    }

    @Override
    public void makeBillPayment(double amt) {
        System.out.println("TRANSFER AMOUNT:"+amt);
    }
}
