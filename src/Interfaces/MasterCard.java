package Interfaces;
//Implementation class
public class MasterCard implements CreditCard{
    @Override
    public void getType() {
        System.out.println("CREDIT CARD TYPE IS MASTER CARD");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("TRANSACTION SUCCESSFUL OF DLR:"+amt);
    }
}
