package Interfaces;
//Implementation class
public class Visa implements CreditCard{
    @Override
    public void getType() {
        System.out.println("CREDIT CARD TYPE IS VISA");
    }

    @Override
    public void withdraw(double amt) {
        System.out.println("TRANSACTION SUCCESSFUL OF RS:"+amt);
    }
}
