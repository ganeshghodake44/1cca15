package Interfaces;
//super interface
@FunctionalInterface
public interface Database {
    void designDatabase(String dbVender);
}
