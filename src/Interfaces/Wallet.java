package Interfaces;
//super class
@FunctionalInterface
public interface Wallet {
    void makeBillPayment(double amt);
}
