package MethodOverloading;

public class MainApp5 {
    public static void main(String[] args) {
        System.out.println(Adder.add(5,10));
        System.out.println(Adder.add(10.2,10.8));
    }
}