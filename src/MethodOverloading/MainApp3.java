package MethodOverloading;

public class MainApp3 {
    //Standard main method
    public static void main(String[] args) {
        System.out.println("MAIN METHOD 1");
        main(25);
        main('G');
    }
    //external methods or non-standard main methods
    public static void main (int a){
        System.out.println("A:"+a);
        System.out.println("MAIN METHOD 2");
    }
    public static void main (char c){
        System.out.println("C:"+c);
        System.out.println("MAIN METHOD 3");
    }
}
