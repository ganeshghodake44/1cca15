package Encapsulation;

public class Employee {
    private int empId=101;

    private double empSalary=12000;

    //read Access
    public int getEmpId() {return empId;}
    //Write Access
    public void setEmpId(int empId) {this.empId=empId;}

    //read Access
    public double getEmpSalary() {return empSalary;}
    //Write Access
    public void setEmpSalary(double empSalary){
        if(empSalary>0){
            this.empSalary=empSalary;
        }else{
            System.out.println("Invalid Amount");
        }
    }
}
