package Encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee e=new Employee();
        //Read private data through getter
        int id=e.getEmpId();
        double salary=e.getEmpSalary();

        System.out.println("ID:"+id);
        System.out.println("SALARY:"+salary);

        //modify private data through setter
        e.setEmpId(201);
        e.setEmpSalary(35000);
        System.out.println("ID:"+e.getEmpId());
        System.out.println("SALARY:"+e.getEmpSalary());
    }
}
