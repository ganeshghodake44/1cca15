package Encapsulation;

public class MainApp2 {
    public static void main(String[] args) {
        Mobile m1=new Mobile();
        System.out.println("COMPANY NAME IS:"+m1.company);

        //ENCAPSULATION
        Mobile.RAM r1=m1.new RAM();
        r1.displayInfo();

        //ENCAPSULATION
        Mobile.Processor p=m1.new Processor();
        p.displayName();
    }
}
