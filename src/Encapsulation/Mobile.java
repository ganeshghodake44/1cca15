package Encapsulation;

public class Mobile {
    String company="Samsung";

    //INNER CLASS
    class RAM{
        void displayInfo(){
            System.out.println("Ram size is 8GB");
        }
    }

    //INNER CLASS
    class Processor{
        void displayName(){
            System.out.println("PROCESSOR NAME IS SNAPDRAGON");
        }
    }
}
