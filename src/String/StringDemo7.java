package String;

import java.util.Scanner;

public class StringDemo7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("enter string");
        String name=sc1.nextLine();
        String userName=name.trim();
        if(!userName.isEmpty()){
            System.out.println("WELCOME:"+userName);
        }else{
            System.out.println("INVALID NAME");
        }
    }
}
