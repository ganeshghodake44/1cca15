package Casting;

public class CastingDemo1 {
    public static void main(String[] args) {
        //MATCHING INFORMATION
        int a=15;
        double b=30;
        System.out.println(a+"\t\t"+b);

        //casting

        int c= (int) 35.85;        //narrowing
        double d=26;              //widening
        System.out.println(c+"\t\t"+d);
    }
}
