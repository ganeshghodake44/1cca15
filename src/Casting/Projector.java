package Casting;
//subclass
public class Projector extends Machine{
    void getType(){
        System.out.println("MACHINE IS PROJECTOR");
    }
    void calculateBill(int qty,double price){
        //gst 10%
        double total=qty*price;
        double finalAmt=total+total*0.1;
        System.out.println("TOTAL AMOUNT:"+finalAmt);
    }
}
