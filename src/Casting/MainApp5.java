package Casting;

public class MainApp5 {
    public static void main(String[] args) {
        Master m1=new Master();              //object superclass
        if(m1 instanceof Central){
            Central c1= (Central) m1;
        }else {
            System.out.println("PROPERTIES NOT FOUND");
        }
    }
}
