package Casting;
//subclass
public class Laptop extends Machine{
    void getType(){
        System.out.println("MACHINE TYPE IS LAPTOP");
    }
    void calculateBill(int qty,double price){
        //15% gst
        double total=qty*price;
        double finalAmt=total+total*0.15;
        System.out.println("TOTAL AMOUNT:"+finalAmt);
    }
}
