package Casting;

import java.util.Scanner;

public class MainApp7 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER SERVICE PROVIDER");
        System.out.println("1:AirAsia\n2:Indigo");
        int choice=sc1.nextInt();
        System.out.println("SELECT ROUTE");
        System.out.println("1:PUNE TO DELHI");
        System.out.println("2:MUMBAI TO CHENNAI");
        System.out.println("3:KOLKATA TO PUNE");
        int routeChoice=sc1.nextInt();
        System.out.println("ENTER NUMBER OF TICKETS");
        int tickets=sc1.nextInt();
        Goibobo g1=null;
        if(choice==1){
            g1=new AirAsia();              //upcasting
        }else if(choice==2){
            g1=new Indigo();               //upcasting
        }else {
            System.out.println("INVALID CHOICE");
        }
        g1.bookTickets(tickets,routeChoice);
    }
}
