package Casting;

public class CastingDemo4 {
    public static void main(String[] args) {
        short s1= (short) 132569;                    //narrowing
        System.out.println(s1);                     //o/p----1497

        long l1=8924525433352522242l;
        int x1= (int) l1;                         //narrowing
        System.out.println(x1);                  //-202733054
    }
}
