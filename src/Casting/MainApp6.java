package Casting;

public class MainApp6 {
    public static void main(String[] args) {
        //upcasting
        Bike b=new ElectricBike();
        b.getType();

        System.out.println("=====================");
        //down-casting
        ElectricBike e= (ElectricBike) b;
        e.getType();
        e.batteryInfo();
    }
}
