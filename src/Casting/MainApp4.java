package Casting;

public class MainApp4 {
    public static void main(String[] args) {
        //upcasting
        Master m1=new Central();                     //upcasting
        m1.test();

        // <------------ downcasting ----------->
        Central c1= (Central) new Master();
        c1.test();
    }
}
