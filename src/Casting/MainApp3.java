package Casting;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("ENTER QTY");
        int qty=sc1.nextInt();
        System.out.println("ENTER PRICE");
        double price=sc1.nextDouble();
        System.out.println("SELECT MACHINE");
        System.out.println("1:Laptop\n2:Projector");
        int choice=sc1.nextInt();
        Machine m1=null;
        if(choice==1){
            m1=new Laptop();               //upcasting
        }
        else if(choice==2){
            m1=new Projector();           //upcasting
        }
         else {
            System.out.println("INVALID CHOICE");
        }
         m1.getType();
         m1.calculateBill(qty,price);
    }
}
