package Casting;

public class MainApp1 {
    public static void main(String[] args) {
        //inheritance single level
        Sample s1=new Sample();
        s1.info();
        s1.test();
        //upcasting
        System.out.println("==============================");
        Demo d1=new Sample();
        d1.test();
    }
}
