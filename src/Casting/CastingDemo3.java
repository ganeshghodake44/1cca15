package Casting;

public class CastingDemo3 {
    public static void main(String[] args) {
        char ch1='J';
        char ch2='P';
        //widening
        int x1=ch1;
        int x2=ch2;
        System.out.println(x1+"\t\t"+x2);
        //narrowing

        int x3=74;
        double x4=80.0;
        char ch3= (char) x3;
        char ch4= (char) x4;
        System.out.println(ch3);
        System.out.println(ch4);
    }
}
