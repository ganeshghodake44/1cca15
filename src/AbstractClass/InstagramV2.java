package AbstractClass;
//SUBCLASS
public class InstagramV2 extends InstagramV1 {
    @Override
    void stories() {
        System.out.println("MAKE STORIES");
    }

    @Override
    void reels() {
        System.out.println("PLAYS REELS");
    }
}
