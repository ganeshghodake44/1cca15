package Polymorphirm;

public abstract class Demo {
    //static And non-static variables
    static int k=20;
    int j=25;

    //non-static abstract and concrete method
    abstract void test();
    void display(){
        System.out.println("DISPLAY METHOD");
    }

    //static concrete method
    static void info(){
        System.out.println("INFO METHOD");
    }

    //constructor
    Demo(){
        System.out.println("CONSTRUCTOR");
    }

    //Static And non-Static Blocks
    static {
        System.out.println("STATIC BLOCKS");
    }
    {
        System.out.println("NON-STATIC BLOCK");
    }
}
