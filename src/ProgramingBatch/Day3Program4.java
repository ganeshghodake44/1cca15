package ProgramingBatch;

public class Day3Program4 {
    public static void main(String[] args){
        int line=5;
        int star=1;
        int space=4;
        int ch=1;
        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){
                System.out.print("   ");
            }
            for(int k=0;k<star;k++){
                if(ch==5){
                    ch=1;
                    System.out.print(" "+ch+" ");
                    ch++;
                }else {
                    System.out.print(" "+ ch +" ");
                    ch++;
                }
            }
            System.out.println( );
            star++;
            space--;
        }
    }
}

