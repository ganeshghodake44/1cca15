package ProgramingBatch;

import java.util.Arrays;

public class ArrayThirdMax {
    public static void main(String[] args) {
        int[]arr={1,2,49,28,12,101};
        System.out.println(Arrays.toString(arr));
        int max1=arr[0];
        int max2=arr[1];
        int max3=arr[2];
        for(int i=0;i<arr.length;i++){
            if(arr[i]>max1) {
                max2 = max1;
                max1 = arr[i];
            }else if(arr[i]>max2 && arr[i]!=max1){
                max3=max2;
                max2=arr[i];
            }else if(arr[i]>max3 && arr[i]!=max2){
                max3=arr[i];
            }
        }
        System.out.println("Max1  "+max1);
        System.out.println("MAX2  "+max2);
        System.out.println("Max3  "+max3);
    }
}
