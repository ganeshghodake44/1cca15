package ProgramingBatch;

public class Day5P4 {
    public static void main(String[] args) {
        for (int i = 10; i < 100; i++) {
            int a = i;
            int temp = a;
            int sum = 0;
            while (a != 0) {
                int r = a % 10;
                sum = sum * 10 + r;
                a /= 10;
            }
            if (sum == temp) {
                System.out.println(sum);
            }
        }
    }
}


