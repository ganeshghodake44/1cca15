package ProgramingBatch;

public class Program24 {
    public static void main(String[] args) {
        int line = 3;
        int star = 4;
        int ch1 = 1;
        for (int a = 0; a < line; a++) {
            for (int b = 0; b < star; b++) {
                System.out.print(ch1 + " ");
            }
            System.out.println();
            for (int k = 0; k < 4; k++) {
                System.out.print("*" + " ");
            }
            System.out.println();
            ch1++;
        }
    }
}
