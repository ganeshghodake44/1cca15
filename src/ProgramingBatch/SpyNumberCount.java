package ProgramingBatch;

public class SpyNumberCount {
    //1124,2411,1142,1421,4211
    public static void main(String[] args) {
        for(int i=10;i<100000;i++){
        int a=i;
        int sum=0;
        int mul=1;
        while(a!=0){
            int r=a%10;
            sum=sum+r;
            mul=mul*r;
            a/=10;
        }
        if(sum==mul) {
            System.out.println(i);
            }
        }
    }
}

