package ProgramingBatch;

import java.util.Arrays;

public class Array1 {
    public static void main(String[] args) {
        int[]arr={1,2,13,4,5};
        System.out.println(Arrays.toString(arr));
        int max=arr[0];
        for(int a:arr){
            if(a>max)
                max=a;
        }
        System.out.println(max);
    }
}
