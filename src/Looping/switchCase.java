package Looping;

import java.util.Scanner;

public class switchCase {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("select language");
        System.out.println("1:JAVA\n2:PYTHON\n3:PHP");
        int choice= sc1.nextInt();
        switch (choice){
            case 1:
                System.out.println("SELECTED JAVA");
                break;
            case 2:
                System.out.println("SELECTED PYTHON");
                break;
            case 3:
                System.out.println("SELECTED PHP");
                break;
            default:
                System.out.println("INVALID CHOICE");
        }
    }
}


