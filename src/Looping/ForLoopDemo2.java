package Looping;

import java.util.Scanner;

public class ForLoopDemo2 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Start Point");
        int Start=sc1.nextInt();
        System.out.println("Enter End Point");
        int End=sc1.nextInt();

        for(int a=Start;a<=End;a++){
            if(a%2!=0){
                System.out.println(a);
            }
        }
    }
}
