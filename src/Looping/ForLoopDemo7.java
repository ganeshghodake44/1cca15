package Looping;

public class ForLoopDemo7 {
    public static void main(String[] args) {
        int lines=3;
        int star=1;
        //outer for loop
        for(int row=1;row<=lines;row++){
            //nested for loop
            for(int col=1;col<=star;col++){
                System.out.print("*"+"\t");
            }
            System.out.println();
            star++;
        }
    }
}
