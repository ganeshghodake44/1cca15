package Gromming;

public class SpyNumbers {
    //1124,2411,1142,1421,4211
    public static void main(String[] args) {
        int a=1124;
        int sum=0;
        int mul=1;
        while(a!=0){
           int r=a%10;
           sum=sum+r;
           mul=mul*r;
           a/=10;
        }
        if(sum==mul) {
            System.out.println(sum +   "IS A SPY NUMBER");
        }else {
            System.out.println(sum+    "IS NOT SPY NUMBER");
        }
    }
}
