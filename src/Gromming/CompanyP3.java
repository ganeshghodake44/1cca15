package Gromming;

public class CompanyP3 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        int ch=4;
        for (int i=0;i<lines;i++){
            int ch1=ch;
            for (int j=0;j<star;j++){
                System.out.print(" "+ch1--+" ");
                if(ch1<0){
                    ch1=0;
                }
            }
            System.out.println();
            ch--;
        }
    }
}
