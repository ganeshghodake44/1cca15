package Gromming;

public class ArmstrongCount {
    //important question
    public static void main(String[] args) {
        for(int i=10;i<10000;i++) {
            int a = i;
            int count = 0;
            int sum = 0;
            int temp = a;
            while (a != 0) {
                a=a/10;
                count++;
            }
            while (temp != 0) {
                int r = temp % 10;
                sum +=Math.pow(r, count);
                temp = temp / 10;
            }
            if (sum == i)
                System.out.println(sum);
        }
    }
}
