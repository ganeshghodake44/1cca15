package Gromming;

//problem state:find the array element pair whose sum is 70 ?

public class ArrayPairSum {
    public static void main(String[] args) {
        int[]arr1={10,20,30,40,50,60};
        for(int a:arr1) {
            for (int b : arr1)
                if (a + b == 70)
                    System.out.println(a + " : " + b);
        }
    }
}
