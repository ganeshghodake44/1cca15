package Gromming;

public class ArraySecondMax {
    public static void main(String[] args) {
        int[]arr={10,20,30,130,40,130,60};
        int max1=arr[0];
        int max2=arr[1];
        for(int a:arr){
            if(a>max1){
                max2=max1;
                max1=a;
            } else if (a>max2 && a!=max1) {
                max2=a;
            }
        }
        System.out.println("MAX1             "+max1);
        System.out.println("MAX2             "+max2);
    }
}
