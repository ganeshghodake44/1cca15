package Gromming;

public class CompanyP10 {
    public static void main(String[] args) {
        int line=6;
        int star=2;
        int ch=1;
        for(int i=0;i<line;i++){
            for (int j=0;j<star;j++){
                if(i%2==0){
                    System.out.print(" "+ch+" ");
                }else {
                    System.out.print(" * ");
                }
            }
            System.out.println();
            ch++;
            star++;
        }
    }
}
