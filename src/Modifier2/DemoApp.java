package Modifier2;

import Modifiers1.Master;

public class DemoApp extends Master {
    public static void main(String[] args) {
        DemoApp d1=new DemoApp();

        System.out.println("C:"+d1.c);      //Projected

        System.out.println("D:"+d1.d);     //public
    }
}
